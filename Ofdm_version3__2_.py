#!/usr/bin/env python
# coding: utf-8

# In[73]:


## scarmbler and FEC part

from commpy.channelcoding import *
from commpy.utilities import *
import numpy as np
from numpy import array
from commpy.modulation import *
import commpy.channelcoding.convcode as cc
import numpy as np
from pyldpc import make_ldpc, encode, decode, get_message
from ModulationPy import *
                
W=input()
def scramble(X):
    a=0
    Y=np.zeros(len(X),dtype=int)
    for j in range (0,len(X)):
        a=X[j]
        Y[j]=X[len(X)-1-j]
        Y[len(X)-j-1]=a
    return Y 
        

def frame(Coding,modulation,input_sequence):
    global d_v,d_c,k,y,x,d,v,l1,modulated,modem,n,m
    m=input_sequence
    if (Coding==(1/2)):
        n = 2*len(m)        
        d_v = len(m)-1             #number of ones per row
        d_c = len(m)             #number of ones per column
        


    if (Coding==(3/4)):
        n = int((len(m)*4)/3)
        d_c = int(n/2)
        d_v = int(n/4)-1
        

    if (Coding==(5/6)):
        n = int((len(m)*6)/5)
        d_c = int(n/2)
        d_v = int(d_c/3)-1

    if (Coding==2/3):
        n = int((len(m)*3)/2)
        d_c = int(n/2)
        d_v = int((d_c)*2/3)-1
        
     
    if (modulation=='BPSK'):
        modem=PSKModem(2)
    



    if (modulation=='QPSK'):
        modem = PSKModem(4, np.pi/4, 
                 bin_input=True,
                 soft_decision=False,
                 bin_output=True)

        
    if (modulation=='16QAM'):
        modem = QAMModem(16,
            bin_output=True,
            soft_decision=False,
            gray_map=True, 
            bin_input=True)


    if (modulation=='64QAM'):

        modem = QAMModem(64,
            bin_output=True,
            soft_decision=False,
            gray_map=True, 
            bin_input=True)
#     if (Coding==(3/4) and modulation=='64QAM'):
#         n = 96
#         d_v = 23
#         d_c = 48
#         modem = QAMModem(64,
#             bin_output=True,
#             soft_decision=False,
#             gray_map=True, 
#             bin_input=True)
    

    snr = 100          #snr would consider later 
    H, G = make_ldpc(n, d_v, d_c, systematic=True, sparse=True)   # H is a regular parity-check matrix
    n,k = G.shape
    Input =m   #random input with size K
    scrambled_input=scramble(Input)                    #scrambled input
    y = encode(G, scrambled_input,snr)

    z=y.flatten()

    i=np.around(z)
    l=np.where(i==1,0,i)
    l1=np.where(l==-1,1,l)
    encoded_input=np.round(l1).astype(int)
    modulated = modem.modulate(encoded_input)
#     print(modulated)
    print("len(modulated)",len(modulated))
#     print("scrambled_input ",scrambled_input)                        
#     print("input",m)
    print("input length",len(m))
#     print("encoded_input",encoded_input)
    print("encoded_input",len(encoded_input))
#     print("n",n)   

frame(1/2,'BPSK',np.random.randint(2, size=int(W)))

# print(modulated)


# In[74]:



#Pylayers
# from __future__ import print_function
import doctest
import pdb
import numpy as np
import numpy.ma as ma
import numpy.linalg as la
import scipy as sp
import scipy.signal as si
import pylab as plt
import struct as stru
import scipy.stats as st
import scipy.optimize as optimize
import numpy.fft as fft
from scipy.io import loadmat
import pylayers.util.pyutil as pyu
import pylayers.signal.bsignal as bs
import pylayers.util.geomutil as geu
import pylayers.antprop.antenna as ant
from pylayers.util.project import *
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D
from scipy.optimize import fmin
import copy
from pylayers.signal.bsignal import *
from pylayers.gis.layout import *
from IPython.display import Image
import os
from mayavi import mlab
from pylayers.simul.link import *
from pylayers.antprop.rays import *
from pylayers.antprop.aarray import *
from pylayers.antprop.channel import *
from pylayers.antprop.signature import *
import pylayers.signal.bsignal as bs
import pylayers.signal.waveform as wvf
from pylayers.antprop.antenna import *
import pylayers.util.mayautil as myu
from pylayers.antprop.coverage import *
from matplotlib.pyplot import *
import pandas as pd
import time
import configparser
from IPython.display import FileLink
import pylayers.util.pyutil as pyu
from IPython.display import Image, HTML, Latex, YouTubeVideo
import numpy as np
import pylayers.mobility.trajectory as traj
from pylayers.mobility.ban.body import *

import matplotlib.pyplot as plt 
from pylayers.antprop.channel import *
import scipy.io

from shapely import speedups
speedups.disable()
get_ipython().run_line_magic('matplotlib', 'inline')
L=Layout('testair1.lay')


L.build()
L.ax
L.Gv.nodes
tx=np.array([1,3,1.4])
rx=np.array([5,3,1.4])
fGHz=np.linspace(2.411,2.433,4*len(modulated))
Aa = Antenna('Gauss',fGHz=fGHz)
Ab = Antenna('Gauss',fGHz=fGHz)
Aa.eval()

Lk = DLink(L=L,
        a=tx,
        b=rx,
        Aa=Aa,            Ab=Ab,
        fGHz=fGHz,save_idx=[0][0])
Lk.eval(force=True)
Lk.evalH(applywav=True)
# print("number of rays =",len(Lk.H.y))
# print(Lk.H)
# print("rays coefficient =",Lk.H.y)


# In[63]:


#Creating OFDM frame

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
from pyphysim.modulators import *
import math
from typing import Optional, Tuple
from pyphysim.modulators.fundamental import *
from pyphysim.channels.fading import *
from pyphysim.channels import *
from pyphysim.modulators.ofdm import *
__all__ = ['OFDM', 'OfdmOneTapEqualizer']
np.set_printoptions(linewidth=70)



OFDM_sample= OFDM(len(modulated)+10, len(modulated)-10, len(modulated))
ModulatedOFDM=OFDM_sample.modulate(modulated)
preparedOFDM=OFDM_sample._prepare_input_signal(ModulatedOFDM)
Signal_before_passing_channel=OFDM_sample._add_CP(preparedOFDM)
Signal_before_passing_channel_serial=Signal_before_passing_channel.flatten()
# Signal_before_passing_channel_serial


# In[64]:


#passing signal through channel

from scipy import signal
from scipy import misc
from scipy import fft
from scipy import ifft

SignalAfterPassingTheChannel=0
for i in range (0,len(Lk.H.y)-1,1):
    Rays=Lk.H.y[i,0,0]
    R1=Signal_before_passing_channel_serial*Rays
    SignalAfterPassingTheChannel=R1+SignalAfterPassingTheChannel
    i=i+1
  


# In[65]:


from numpy import sum,isrealobj,sqrt
from numpy.random import standard_normal
from pyldpc import make_ldpc, encode, decode, get_message
import numpy as np
from pyldpc import make_ldpc, decode, get_message, encode
from matplotlib import pyplot as plt

def awgn(s,SNRdB,L=1):
    """
    AWGN channel
    Add AWGN noise to input signal. The function adds AWGN noise vector to signal 's' to generate a resulting signal vector 'r' of specified SNR in dB. It also
    returns the noise vector 'n' that is added to the signal 's' and the power spectral density N0 of noise added
    Parameters:
        s : input/transmitted signal vector
        SNRdB : desired signal to noise ratio (expressed in dB) for the received signal
        L : oversampling factor (applicable for waveform simulation) default L = 1.
    Returns:
        r : received signal vector (r=s+n)
"""
    gamma = 10**(SNRdB/10) #SNR to linear scale
    if s.ndim==1:# if s is single dimensional vector
        P=L*sum(abs(s)**2)/len(s) #Actual power in the vector
    else: # multi-dimensional signals like MFSK
        P=L*sum(sum(abs(s)**2))/len(s) # if s is a matrix [MxN]
    N0=P/gamma # Find the noise spectral density
    if isrealobj(s):# check if input is real/complex object type
        n = sqrt(N0/2)*standard_normal(s.shape) # computed noise
    else:
        n = sqrt(N0/2)*(standard_normal(s.shape)+1j*standard_normal(s.shape))
    r = s + n # received signal
    return r






errors=[]
snrs=np.arange(-8,42,2,dtype=int)
for SNR in snrs:
    ReceivedSignal_with_noise=awgn(SignalAfterPassingTheChannel,SNR)
    ReceivedSignal_witouthCP=OFDM_sample._remove_CP(ReceivedSignal_with_noise)
    ReceivedSignal_prepared=OFDM_sample._prepare_decoded_signal(ReceivedSignal_witouthCP)
    ReceivedSignal_demodulated=OFDM_sample.demodulate(ReceivedSignal_prepared)
    demodulated=modem.demodulate(ReceivedSignal_demodulated)
    n = n
    d_v = d_v
    d_c = d_c
    snr = 100
    H, G = make_ldpc(n, d_v, d_c, systematic=True, sparse=True)
    d = decode(H, demodulated, snr)
    Output = get_message(G, d)
    descrambled_Output=scramble(Output)
    B=descrambled_Output-Input
    error=np.count_nonzero(B)/len(Input)
    errors.append(error)
    print(errors)
    print(B)
    
plt.figure()
plt.plot(snrs, errors, color="indianred")
plt.ylabel("Bit error rate")
plt.xlabel("SNR")
plt.show()


# In[ ]:




